<?php


$path = dirname( __FILE__ ) . '/jsonstreamingparser-master';
require_once $path . '/src/JsonStreamingParser/Listener.php';
require_once $path . '/example/InMemoryListener.php';
//require_once $path . '/src/JsonStreamingParser/Listener/IdleListener.php';
require_once $path . '/src/JsonStreamingParser/Parser.php';

function assertParsesCorrectly( $file ) {
// Parse using an InMemoryListener instance
	$listener	 = new InMemoryListener();
	$stream		 = fopen( $file, 'r' );
	try {
		$parser = new JsonStreamingParser_Parser( $stream, $listener );
		$parser->parse();
		fclose( $stream );
	} catch ( Exception $e ) {
		fclose( $stream );
		throw $e;
	}
	$actual = $listener->get_json();
}

$url = 'https://datafiniti-voltron-results.s3.amazonaws.com/zdcxvjvtcd6l3sacnkmjephcrib84lqw/98255_1.txt?AWSAccessKeyId=AKIAJIWA5YA2GA6JN7DA&Expires=1433334584&Signature=K%2FK9IOtwNIKA3bhOOvcW08%2Fw9rQ%3D&response-content-disposition=attachment';
assertParsesCorrectly( $url );

