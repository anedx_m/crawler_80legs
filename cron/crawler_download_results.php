<?php

set_time_limit( 0 );

$path = dirname( dirname( __FILE__ ) ) . '/jsonstreamingparser-master';
require_once $path . '/src/JsonStreamingParser/Listener.php';
require_once $path . '/src/JsonStreamingParser/Parser.php';
require_once $path . '/src/JsonStreamingParser/Listener/IdleListener.php';



include dirname( dirname( __FILE__ ) ) . '/config.php';

Helper::system_down_check();


$crawler = new Crawler();


$db				 = DB::getInstance();
$insert_domain	 = $db->prepare( 'INSERT IGNORE INTO domains_all (domain,date_found) VALUES (:domain,CURRENT_TIMESTAMP);' );

function callback( $obj ) {
	global $crawler;
	$value = $obj[ 'value' ];
	if ( isset( $value[ 'result' ] ) ) {

		$result		 = json_decode( $value[ 'result' ] );
		$list_urls	 = array();
		foreach ( $result->domainCount as $domain => $count ) {
			$list_urls[] = $domain;
		}
		$crawler->addResultsToDomains( $list_urls );
		echo " -= {$value[ 'url' ]}: " . count( $list_urls ) . " =- ";
	}
}

function parsesJSON( $url ) {
// Parse using an InMemoryListener instance
	$listener	 = new InMemoryListener( 'callback' );
	$stream		 = fopen( $url, 'r' );
	try {
		$parser = new JsonStreamingParser_Parser( $stream, $listener );
		$parser->parse();
		fclose( $stream );
	} catch ( Exception $e ) {
		fclose( $stream );
		throw $e;
	}
}

$process = new Process( $db );
$process->removeDeadProcesses( 60 );
Helper::system_down_check();
$type	 = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '{check crawl status} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$r	 = $db->query( 'SELECT * FROM crawls WHERE status="COMPLETED"' );
$r	 = $r->fetchAll( PDO::FETCH_ASSOC );


foreach ( $r as $row ) {
	$results = $crawler->getResultsCrawl( $row[ 'title' ] );
	//var_dump( $results );
	if ( $results ) {
		$results = json_decode( $results );
		foreach ( $results as $url ) {


			parsesJSON( $url );

			$r = $db->query( 'DELETE FROM crawls WHERE id=' . $row[ 'id' ] );
			$crawler->deleteUrlList( $row[ 'title' ] );
		}
	} else {
		$r = $db->query( 'DELETE FROM crawls WHERE id=' . $row[ 'id' ] );
		$crawler->deleteUrlList( $row[ 'title' ] );
	}
	$process->updateTime( $pid );
	Helper::system_down_check();
}
$r = $db->query( 'DELETE FROM crawls WHERE status="CANCELED"' );


$process->endProcess( $pid );
die( "Done" );

