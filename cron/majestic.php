<?php

set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) .'/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( 60 );

Helper::system_down_check();

$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '{majestic} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

//$MajesticAPI	 = new MajesticAPI();
$MajesticAPI = new MajesticAPI();

// get wait settings
$wait		 = Helper::getSetting( 'ms_wait_time' );
$a_wait		 = explode( '-', $wait );
$min		 = trim( $a_wait[ 0 ] );
if ( empty( $a_wait[ 1 ] ) )
	$a_wait[ 1 ] = $min;
$max		 = trim( $a_wait[ 1 ] );
$limit = 25;
while ( $pr = $MajesticAPI->processNextDomains($limit) ) {
	$process->updateTime( $pid );
	$s = 2000000;
	// wait 1.2 seconds  = 50 requests per minute
	echo "wait $s mksec";
	usleep( $s );
	Helper::system_down_check();
}

$process->endProcess( $pid );
die( "Done" );
