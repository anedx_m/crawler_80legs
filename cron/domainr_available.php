<?php

set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . '/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( 60 );

Helper::system_down_check();

$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '1 {whois_avail} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );

$process->addProcess( $pid, $type );

$DomainrAPI		 = new DomainrAPI();
$domainr_timeout = Helper::getSetting( 'domainr_timeout' );

$limit	 = 1;

while ( $pr		 = $DomainrAPI->processTakenNextDomains( $limit ) ) {
	$process->updateTime( $pid );
	echo "wait 1 sec";
	sleep( 1 );
	Helper::system_down_check();
	
}

$process->endProcess( $pid );
die( "Done" );
