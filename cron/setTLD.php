<?php

set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . '/config.php';

$db = DB::getInstance();

$crawler = new Crawler();

$r = $db->query( 'SELECT * FROM domains_avail' );
if ( $r ) {
	while ( $row = $r->fetch() ) {
		$tld = $crawler->getTLD( $row[ 'domain' ] );
		$pr	 = $db->prepare( "UPDATE domains_avail SET tld=:tld WHERE id=:domain_id" );
		$pr->execute( array( ':domain_id' => $row[ 'id' ], ':tld' => $tld ) );
	}
}

$r = $db->query( 'SELECT * FROM domains_all' );
if ( $r ) {
	while ( $row = $r->fetch() ) {
		$tld = $crawler->getTLD( $row[ 'domain' ] );
		$pr	 = $db->prepare( "UPDATE domains_all SET tld=:tld WHERE id=:domain_id" );
		$pr->execute( array( ':domain_id' => $row[ 'id' ], ':tld' => $tld ) );
	}
}
die('DONE');