<?php

set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) .'/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( 60 );

Helper::system_down_check();

$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '{check crawl status} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$r	 = $db->query( 'SELECT COUNT(*) FROM crawls' );
$r	 = $r->fetch( PDO::FETCH_COLUMN );

$tlds			 = Helper::getSetting( 'whois_tld_priority' );
$order_by_tlds	 = '';
if ( $tlds ) {
	$tlds	 = explode( ' ', $tlds );
	$tlds    = array_reverse($tlds);
	$s_tlds	 = array_map( function($value) {
		return "'$value'";
	}, $tlds );
	$tlds			 = implode( ', ', $s_tlds );
	$order_by_tlds	 = "ORDER BY FIELD(tld, $tlds,'') DESC";
}


$domains_per_crawl	 = Helper::getSetting( 'domains_per_crawl' );
$max_crawls			 = Helper::getSetting( 'max_crawls' );
$pr					 = $db->prepare( 'UPDATE domains_all SET was_crawled=1 WHERE id=:domain_id' );
var_dump( $r, $max_crawls );
if ( $r < $max_crawls ) {
	for ( $i = 0; $i < $max_crawls - $r; $i++ ) {
		$urls	 = $db->query( "SELECT * FROM domains_all WHERE was_crawled<>1 $order_by_tlds LIMIT " . $domains_per_crawl );
		$urls	 = $urls->fetchAll( PDO::FETCH_ASSOC );

		if ( count( $urls ) == 0 ) {
			break;
		}
		$list_urls = array();
		foreach ( $urls as $url ) {
			$list_urls[] = 'http://' . $url[ 'domain' ];
			$pr->execute( array( ':domain_id' => $url[ 'id' ] ) );
		}
		$crawler = new Crawler();
		$crawler->uploadUrlsAddCrawl( $list_urls );
		$process->updateTime( $pid );
		sleep( 1 );
		Helper::system_down_check();
	}
}

$process->endProcess( $pid );
die( "Done" );

