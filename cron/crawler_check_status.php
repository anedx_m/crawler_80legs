<?php

set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) . '/config.php';


Helper::system_down_check();

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( 60 );

$type = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '{check crawl status} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );
$process->addProcess( $pid, $type );

$r	 = $db->query( 'SELECT * FROM crawls' );
$r	 = $r->fetchAll( PDO::FETCH_ASSOC );

$crawler = new Crawler();
foreach ( $r as $row ) {
	$cr	 = $crawler->getCrawlStatus( $row[ 'title' ] );
	$cr	 = json_decode( $cr, true );
//	var_dump( $cr );
	$links_found = $cr['urls_crawled'];
	$db	 = DB::getInstance();
	$pr	 = $db->prepare( 'UPDATE crawls SET links_found	=:links_found	 WHERE id=:crawl_id ' );
	$r	 = $pr->execute( array( ':links_found' => $links_found, ':crawl_id' => $row[ 'id' ] ) );
//	var_dump($pr->errorInfo());
	if ( $cr[ 'status' ] != $row[ 'status' ] ) {
		$crawler->updateCrawlStatus( $row[ 'id' ], $cr[ 'status' ] );
	}


	usleep( 300000 );
	Helper::system_down_check();
}

$process->endProcess( $pid );
die( "Done" );

