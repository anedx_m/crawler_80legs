<?php
set_time_limit( 0 );
include dirname( dirname( __FILE__ ) ) .'/config.php';

$db		 = DB::getInstance();
$process = new Process( $db );
$process->removeDeadProcesses( 60 );

Helper::system_down_check();

$type	 = basename( __FILE__ );
if ( $process->getCountLive( $type ) >= 1 ) {
	die( '1 {whois_avail} bots aready run!' );
}

$pid = md5( time() . mt_rand( 0, 1000000 ) );

$process->addProcess( $pid, $type );
	
$WhoisAPI	 = new WhoisAPIAvailable();
$taken_per_minute	 = Helper::getSetting( 'taken_per_minute' );
while ( $pr		 = $WhoisAPI->processTakenNextDomain(  ) ) {
	$process->updateTime( $pid );
	$s = (60 / $taken_per_minute)*1000000;
	// wait 1.2 seconds  = 50 requests per minute
	echo "wait $s mksec";
	usleep($s);
	Helper::system_down_check();
}

$process->endProcess( $pid );
die( "Done" );
