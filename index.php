<?php

include 'config.php';

$settings		 = Helper::getOption( 'settings' );
$valid_passwords = array( "admin" => Helper::getValue( $settings, 'password' ) );
$valid_users	 = array_keys( $valid_passwords );

$user	 = isset( $_SERVER[ 'PHP_AUTH_USER' ] ) ? $_SERVER[ 'PHP_AUTH_USER' ] : '';
$pass	 = isset( $_SERVER[ 'PHP_AUTH_PW' ] ) ? $_SERVER[ 'PHP_AUTH_PW' ] : '';

$validated = (in_array( $user, $valid_users )) && ($pass == $valid_passwords[ $user ]);

if ( !$validated ) {
	header( 'WWW-Authenticate: Basic realm=""' );
	header( 'HTTP/1.0 401 Unauthorized' );
	die( "Not authorized" );
}


$nameController	 = isset( $_GET[ 'c' ] ) ? $_GET[ 'c' ] : DEFAULT_CONTROLLER;
$nameController	 = $nameController . 'Controller';
$action			 = isset( $_GET[ 'act' ] ) ? $_GET[ 'act' ] : '';


if ( class_exists( $nameController ) ) {
	$controller = new $nameController();

	if ( $action ) {
		$method = 'action' . $action;
	} else {
		$method = 'action' . $controller->{'defaultAction'};
	}
	if ( method_exists( $nameController, $method ) ) {
		$controller->$method();
	} else {
		echo "Action not found";
	}
} else {
	echo "Controller $nameController not found";
}

