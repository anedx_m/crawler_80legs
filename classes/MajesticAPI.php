<?php

class MajesticAPI extends SCurl {

	public function getNextDomains( $limit ) {
		$db			 = Db::getInstance();
		$moz_da		 = 0;
		$pr			 = $db->prepare( "
		UPDATE domains_all SET whois_date = NULL
			FROM  (    SELECT id   FROM   domains_offline WHERE  whois_date IS NULL AND moz_da>:moz_da LIMIT :limit FOR UPDATE) sub
			WHERE  domains_all.id = sub.id
		RETURNING domains_all.domain as url, domains_all.id as domain_id" ); //debug 
		$r			 = $pr->execute( array( ':limit' => $limit, ':moz_da' => $moz_da ) );
		$start_urls	 = $pr->fetchAll( PDO::FETCH_NAMED );

		return $start_urls;
	}

	public function processNextDomains( $limit ) {
		$ms_api_key = Helper::getSetting( 'ms_api_key' );

		$db							 = DB::getInstance();
		$query_select_topic_id		 = $db->prepare( "SELECT id FROM majestic_topics WHERE title=:title" );
		$query_insert_mj_topic		 = $db->prepare( "INSERT INTO majestic_topics (title) VALUES (:title)" );
		$query_insert_avail_topic	 = $db->prepare( "INSERT IGNORE INTO domains_avail_topics (domain_id,topic_id,trust) VALUES (:domain_id,:topic_id,:trust)" );
		$query_update_avail			 = $db->prepare( "UPDATE domains_avail SET majestic_check_date=CURRENT_TIMESTAMP,mj_trust=:mj_trust, mj_citation=:mj_citation, mj_ref_domains=:mj_ref_domains, mj_ext_back_links=:mj_ext_back_links WHERE id=:domain_id" );
		$query_update_testing 		 = $db->prepare( "UPDATE domains_avail SET majestic_check_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );

		$db			 = DB::getInstance();
		$pr_domains	 = $db->prepare( "SELECT * FROM domains_avail WHERE domainr_available=1 AND majestic_check_date IS NULL LIMIT $limit" ); //debug 
		$r			 = $pr_domains->execute();
		$domains	 = $pr_domains->fetchAll( PDO::FETCH_NAMED );
		$pr_domains->closeCursor();
		var_dump( $domains );
		if ( !count( $domains ) ) {
			return 0;
		}

		$DesiredTopics	 = 30;
		
		$url			 = $this->getMajesticUrl( $domains, $ms_api_key,$DesiredTopics );
		var_dump( $url );
		
		
		foreach ( $domains as $domain ) 
			$query_update_testing->execute( array( ':domain_id' => $domain[ 'id' ] ));

		$content	 = $this->request( $url );
		$curl_info	 = $this->getLastCurlInfo();
		if ( $curl_info[ 'http_code' ] == 200 ) {
			$data = json_decode( $content, true );
			var_dump( $data );
			if ( $data[ 'Code' ] == 'OK' ) {
				$DataTables		 = $data[ 'DataTables' ];
				$mj_data		 = $DataTables[ 'Results' ][ 'Data' ];
				$mj_topics_count = $DataTables[ 'Results' ][ 'Headers' ][ 'TopicsCount' ];
				foreach ( $mj_data as $mj ) {
					if ( $mj[ 'ResultCode' ] != 'OK' )
						continue;
					$domain		 = $mj[ 'Item' ];
					$pr_domain	 = $db->prepare( "SELECT id FROM domains_avail WHERE domain=:domain" );
					$r			 = $pr_domain->execute( array( ':domain' => $domain ) );
					if ( !$r )
						continue;

					$domain_id = $pr_domain->fetchColumn();
					$pr_domain->closeCursor();

					$mj_trust			 = $mj[ 'TrustFlow' ];
					$mj_citation		 = $mj[ 'CitationFlow' ];
					$mj_ref_domains		 = $mj[ 'RefDomains' ];
					$mj_ext_back_links	 = $mj[ 'ExtBackLinks' ];
					$query_update_avail->execute( array( ':domain_id' => $domain_id, ':mj_trust' => $mj_trust, ':mj_citation' => $mj_citation, ':mj_ref_domains' => $mj_ref_domains, ':mj_ext_back_links' => $mj_ext_back_links ) );

					for ( $i = 0; $i < $mj_topics_count; $i++ ) {
						$topic_title = $mj[ "TopicalTrustFlow_Topic_$i" ];
						$topic_value = $mj[ "TopicalTrustFlow_Value_$i" ];
						if ( !$topic_value )
							continue;

						$r			 = $query_select_topic_id->execute( array( 'title' => $topic_title ) );
						$mj_topic_id = '';
						if ( $r ) {
							$mj_topic_id = $query_select_topic_id->fetchColumn();
							$query_select_topic_id->closeCursor();
						}
						if ( !$mj_topic_id ) {
							$r = $query_insert_mj_topic->execute( array( ':title' => $topic_title ) );
							$query_insert_mj_topic->closeCursor();
							if ( $r ) {
								$mj_topic_id = $db->lastInsertId();
							} else {
								continue;
							}
						}
						$query_insert_avail_topic->execute( array( ':domain_id' => $domain_id, ':topic_id' => $mj_topic_id, ':trust' => $topic_value ) );
						$query_insert_avail_topic->closeCursor();
					}
				}
			}
		}


		return count( $domains );
	}

	public function getMajesticUrl( $domains, $key, $DesiredTopics ) {
		$arr_d	 = array();
		$i		 = 0;
		foreach ( $domains as $domain ) {
			$arr_d[ 'item' . $i ] = $domain[ 'domain' ];
			$i++;
		}
		$items = http_build_query( $arr_d );

		return "http://api.majestic.com/api/json?app_api_key=$key&cmd=GetIndexItemInfo&items=$i&$items&datasource=fresh&DesiredTopics=$DesiredTopics";
	}

}
