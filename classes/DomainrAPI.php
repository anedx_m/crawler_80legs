<?php

class DomainrAPI extends SCurl {

	public function processTakenNextDomains( $limit ) {
		$mashape_key = Helper::getSetting( 'mashape_key' );

		$tlds			 = Helper::getSetting( 'whois_tld_priority' );
		$order_by_tlds	 = '';
		if ( $tlds ) {
			$tlds	 = explode( ' ', $tlds );
			$tlds    = array_reverse($tlds);
			$s_tlds	 = array_map( function($value) {
				return "'$value'";
			}, $tlds );
			$tlds			 = implode( ', ', $s_tlds );
			$order_by_tlds	 = "ORDER BY FIELD(tld, $tlds,'') DESC";
		}

		$db	 = DB::getInstance();

		$query_update_avail = $db->prepare( "UPDATE domains_avail SET domainr_available=1, domainr_check_time=CURRENT_TIMESTAMP WHERE domain=:domain" );
		$query_update_not_avail  = $db->prepare( "UPDATE domains_avail SET domainr_available=0, domainr_check_time=CURRENT_TIMESTAMP WHERE domain=:domain" );
		$query_update_not_avail_mask  = $db->prepare( "UPDATE domains_avail SET domainr_available=0, domainr_check_time=CURRENT_TIMESTAMP WHERE domain LIKE :domain" );
		
		$query_select_untested = $db->prepare( "SELECT * FROM domains_avail WHERE domainr_check_time IS NULL $order_by_tlds LIMIT $limit" ); //debug 
		$r = $query_select_untested->execute();
		$domains = $query_select_untested->fetchAll( PDO::FETCH_NAMED );
		
		if ( $domains ) {
			$url		 = $this->DomainrUrl( $domains, $mashape_key );
			var_dump( $url );
			
			foreach ( $domains as $domain ) {
				$query_update_avail->execute( array( ':domain' => $domain[ 'domain' ]) );
			}
			//die("ok1?");
			
			$curl_h		 = array( "Accept-Encoding: gzip,deflate" );
			$content	 = $this->request( $url );
			$curl_info	 = $this->getLastCurlInfo();
//			var_dump( $content );
			if ( $curl_info[ 'http_code' ] == 200 ) {
				$db		 = DB::getInstance();
				$data	 = json_decode( $content, true );
				var_dump( $data );
				foreach ( $data[ 'status' ] as $row ) {
					$domain = $row[ 'domain' ];
					if ( $row[ 'summary' ] !== 'inactive' ) {
						$query_update_not_avail->execute( array( ':domain' => $domain ) );
						$query_update_not_avail_mask->execute( array( ':domain' => '%.'.$domain ) );
					}
					var_dump( $row );
				}
			} else {
				var_dump( $curl_info );
			}
		}
		return count( $domains );
	}

	public function DomainrUrl( $domains, $mashape_key ) {
		$arr_domains = array();
		foreach ( $domains as $domain ) {
			$arr_domains[] = $domain[ 'domain' ];
		}
		$str_domains = implode( ',', $arr_domains );
		return "https://domainr.p.mashape.com/v2/status?domain=$str_domains&mashape-key=$mashape_key";
	}

}
