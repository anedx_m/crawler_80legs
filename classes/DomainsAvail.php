<?php

class DomainsAvail extends Table {

	public function getSortableColumns() {
		return array( 'domain', 'tld', 'domainr_check_time', 'mj_trust', 'mj_citation', 'mj_ref_domains', 'mj_ext_back_links' );
	}

	public function getFields() {
		return array( 'domain' => 'Domain', 'tld' => 'TLD', 'domainr_check_time' => 'Date Found', 'mj_trust' => 'Trust', 'mj_citation' => 'Citation', 'mj_ref_domains' => 'Referring Domains', 'mj_ext_back_links' => 'External Backlinks', 'topics' => 'Topics' );
	}

	public function display() {
		$this->max_visible_pages = 10;
		$this->createTable();
	}

	public function processingField( $data_row, $field ) {
		switch ( $field ) {
			case 'domain':
				return '<a href="http://' . $data_row[ $field ] . '" target=_blank>' . $data_row[ $field ] . '</a>';
				break;
			case 'mj_ref_domains':
				return number_format( $data_row[ $field ] );
				break;
			case 'mj_ext_back_links':
				return number_format( $data_row[ $field ] );
				break;
			case 'topics':
				$limit	 = Helper::getSetting( 'display_first_x_topics' );
				$limit	 = empty( $limit ) ? 30 : $limit;
				$db		 = DB::getInstance();
				$pr		 = $db->prepare( 'SELECT * FROM domains_avail_topics as da INNER JOIN majestic_topics AS mj ON mj.id=da.topic_id WHERE domain_id=:domain_id ORDER BY da.trust DESC LIMIT :limit' );
				$pr->execute( array( ':domain_id' => $data_row[ 'id' ], ':limit' => $limit ) );
				$topics	 = $pr->fetchAll( PDO::FETCH_ASSOC );
				$r		 = '';
				foreach ( $topics as $topic ) {
					$r .="<div>{$topic[ 'trust' ]}  {$topic[ 'title' ]}</div>";
				}
				return $r;
				break;
			default:
				return $data_row[ $field ];
		}
	}

	public function getData() {
		$db = DB::getInstance();

		$current_page = $this->getCurrentPage();

		$where		 = '';
		$params		 = Helper::getRequest( 'params' );
		$search		 = Helper::getValue( $params, 'search' );
		$date_from	 = Helper::getValue( $params, 'date_from' );
		$date_to	 = Helper::getValue( $params, 'date_to' );
		$max_row	 = Helper::getValue( $params, 'max_row' );
		$topic_id	 = Helper::getValue( $params, 'topic' );

		$prepare_params = array();
		if ( $search ) {
			$where						 = "WHERE domain LIKE :search";
			$prepare_params[ ':search' ] = "%$search%";
		}

		if ( $where ) {
			$where .= " AND domainr_available=1";
		} else {
			$where = "WHERE domainr_available=1";
		}

		if ( $date_from ) {
			if ( $where ) {
				$where .= " AND domainr_check_time>=:date_from";
			} else {
				$where = "WHERE domainr_check_time>=:date_from";
			}
			$prepare_params[ ':date_from' ] = $date_from;
		}
		if ( $date_to ) {
			if ( $where ) {
				$where .= " AND domainr_check_time<=:date_to";
			} else {
				$where = "WHERE domainr_check_time<=:date_to";
			}
			$prepare_params[ ':date_to' ] = $date_to;
		}
		$topic = '';
		if ( $topic_id ) {
			$topic = "INNER JOIN domains_avail_topics as dt ON dt.domain_id=domains_avail.id";
			if ( $where ) {
				$where .= " AND dt.topic_id=:topic_id";
			} else {
				$where = "WHERE dt.topic_id=:topic_id";
			}
			$prepare_params[ ':topic_id' ] = $topic_id;
		}


		$order_by		 = $this->order_by;
		$order_by_sql	 = '';
		$direction		 = $this->direction;
//		var_dump( $order_by, $direction );
//		if ( $direction == 'DESC' ) {
//			$direction = 'ASC';
//		} else {
//			$direction = 'DESC';
//		}
		$sc				 = $this->getSortableColumns();
		if ( $order_by AND in_array( $order_by, $sc ) ) {
			$domain			 = $order_by == 'domain' ? '' : ',domain';
			$order_by_sql	 = "ORDER BY $order_by $direction $domain";
		}

		$limit		 = $this->limit;
		$limit_sql	 = '';

		if ( $max_row ) {
			$limit_sql					 = "LIMIT :limit";
			$prepare_params[ ':limit' ]	 = $max_row;
		} elseif ( $limit ) {
			$limit_sql					 = "LIMIT :limit OFFSET :offset";
			$prepare_params[ ':limit' ]	 = $limit;
			$prepare_params[ ':offset' ] = ($current_page - 1) * $limit;
		}
		$sql	 = "SELECT * FROM domains_avail $topic $where $order_by_sql $limit_sql";
		$jobs	 = $db->prepare( $sql );
		$r		 = $jobs->execute( $prepare_params );
//		var_dump( $_REQUEST, $order_by, $sql, $jobs, $prepare_params, $jobs->errorInfo() );
//		die();
		$jobs	 = $jobs->fetchAll( PDO::FETCH_NAMED );

		unset( $prepare_params[ ':limit' ] );
		unset( $prepare_params[ ':offset' ] );
		$amount			 = $db->prepare( 'SELECT COUNT(*) as amount FROM domains_avail ' . $topic . ' ' . $where );
		$r				 = $amount->execute( $prepare_params );
		$amount			 = $amount->fetch( PDO::FETCH_COLUMN );
		//-=-=-=-=-=-=-=-=-
		$this->amount	 = $amount;
		//-=-=-=-=-=-=-=-=-
		return $jobs;
	}

}
