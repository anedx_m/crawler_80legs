<?php

class Crawler extends Crawler80Legs {

	private $regex_tld_masks;

	public function __construct() {
		parent::__construct();
		$this->initTLDs();
	}

	public function addCrawl( $crawl_name, $list_name ) {

		$max_depth			 = (int) Helper::getSetting( 'max_depth' );
		$max_urls_per_crawl	 = (int) Helper::getSetting( 'max_urls_per_crawl' );

		$r = $this->createCrawl( $crawl_name, 'DomainCollector.js', $list_name, '', $max_depth, $max_urls_per_crawl );

		$httpcode = $this->getLastHTTPCODE();

		if ( $httpcode == 204 ) {
			$cr		 = $this->getCrawlStatus( $crawl_name );
			$cr		 = json_decode( $cr, true );
			$status	 = $cr[ 'status' ];
			$db		 = DB::getInstance();
			$sql	 = 'INSERT INTO crawls (title,status) VALUES (:title,:status)';
			$up		 = $db->prepare( $sql );
			$r		 = $up->execute( array( ':title' => $crawl_name, ':status' => $status ) );

			if ( $r )
				return $db->lastInsertId();
		}

		$this->deleteUrlList( $list_name );
//		die();
		return false;
	}

	public function uploadUrlsAddCrawl( $arr_urls ) {
		$postfix	 = $this->getPostfix();
		$prefix		 = Helper::getSetting( 'crawl_prefix' );
		$list_name	 = $prefix . $postfix;
		$crawl_name	 = $list_name;

		$r			 = $this->uploadUrls( $list_name, $arr_urls );
		$httpcode	 = $this->getLastHTTPCODE();

		if ( $httpcode == 204 ) {
			$this->addCrawl( $crawl_name, $list_name );
		} else {
			
		}
	}

	public function getPostfix() {
		$db		 = DB::getInstance();
		$sql	 = 'SHOW TABLE STATUS LIKE "crawls"';
		$up		 = $db->prepare( $sql );
		$r		 = $up->execute();
		$postfix = '0';
		if ( $r ) {
			$t		 = $up->fetch( PDO::FETCH_ASSOC );
			$id		 = $t[ 'Auto_increment' ];
			if ( !empty( $id ) )
				$postfix = $id;
		}
		$up->closeCursor();
		return $postfix;
	}

	public function updateCrawlStatus( $crawl_id, $new_status ) {
		$db	 = DB::getInstance();
		$pr	 = $db->prepare( 'UPDATE crawls SET status=:status WHERE id=:crawl_id ' );
		$r	 = $pr->execute( array( ':status' => $new_status, ':crawl_id' => $crawl_id ) );
		return $r;
	}

	public function addResultsToDomains( $list_urls ) {
		$db	 = DB::getInstance();
		$pr	 = $db->prepare( 'INSERT IGNORE INTO domains_all (domain,date_found,tld) VALUES (:domain,CURRENT_TIMESTAMP,:tld);' );
		foreach ( $list_urls as $url ) {
			$domain	 = $this->extractDomain( $url );
			$domain	 = $this->mainDomain( $domain );
			if ( $domain ) {
				$tld = $this->getTLD( $domain );
				$pr->execute( array( ':domain' => $domain, ':tld' => $tld ) );
			}
		}
	}

	private function mainDomain( $domain ) {
		if ( !$domain )
			return; // zune , skype links?

		$tld = $this->getTLD( $domain );
		if ( !$tld )
			return; // wrong domains

		$date_found = date( 'Y-m-d H:i:s' );

		// get MAIN domain only, news.bbc.co.uk   ->  bbc.co.uk 
		$parts_num_get		 = 1 + count( explode( ".", $tld ) );
		$parts				 = explode( ".", $domain );
		$main_domain_parts	 = array_slice( $parts, count( $parts ) - $parts_num_get );
		$main_domain		 = join( ".", $main_domain_parts );
		$main_domain		 = mb_strtolower( $main_domain, 'UTF-8' );
		return $main_domain;
	}

	private function extractDomain( $url ) {
		$parts_url = self::splitURL( $url );
		if ( $parts_url[ 'protocol' ] != 'http://' AND $parts_url[ 'protocol' ] != 'https://' )
			return '';
		return trim( $parts_url[ 'host' ] );
	}

	private static function splitURL( $url ) {
		// Protokoll der URL hinzuf�gen (da ansonsten parse_url nicht klarkommt)
		if ( !preg_match( "#^[a-z]+://# i", $url ) )
			$url = "http://" . $url;

		$parts = @parse_url( $url );

		if ( !isset( $parts ) )
			return null;

		$protocol		 = $parts[ "scheme" ] . "://";
		$host			 = (isset( $parts[ "host" ] ) ? $parts[ "host" ] : "");
		$path			 = (isset( $parts[ "path" ] ) ? $parts[ "path" ] : "");
		$query			 = (isset( $parts[ "query" ] ) ? "?" . $parts[ "query" ] : "");
		$auth_username	 = (isset( $parts[ "user" ] ) ? $parts[ "user" ] : "");
		$auth_password	 = (isset( $parts[ "pass" ] ) ? $parts[ "pass" ] : "");
		$port			 = (isset( $parts[ "port" ] ) ? $parts[ "port" ] : "");

		// File
		preg_match( "#^(.*/)([^/]*)$#", $path, $match ); // Alles ab dem letzten "/"
		if ( isset( $match[ 0 ] ) ) {
			$file	 = trim( $match[ 2 ] );
			$path	 = trim( $match[ 1 ] );
		} else {
			$file = "";
		}

		// Der Domainname aus dem Host
		// Host: www.foo.com -> Domain: foo.com
		$parts = @explode( ".", $host );
		if ( count( $parts ) <= 2 ) {
			$domain = $host;
		} else if ( preg_match( "#^[0-9]+$#", str_replace( ".", "", $host ) ) ) { // IP
			$domain = $host;
		} else {
			$pos	 = strpos( $host, "." );
			$domain	 = substr( $host, $pos + 1 );
		}

		// DEFAULT VALUES f�r protocol, path, port etc. (wenn noch nicht gesetzt)
		// Wenn Protokoll leer -> Protokoll ist "http://"
		if ( $protocol == "" )
			$protocol = "http://";

		// Wenn Port leer -> Port setzen auf 80 or 443
		// (abh�ngig vom Protokoll)
		if ( $port == "" ) {
			if ( strtolower( $protocol ) == "http://" )
				$port	 = 80;
			if ( strtolower( $protocol ) == "https://" )
				$port	 = 443;
		}

		// Wenn Pfad leet -> Pfad ist "/"
		if ( $path == "" )
			$path = "/";

		// R�ckgabe-Array
		$url_parts[ "protocol" ] = $protocol;
		$url_parts[ "host" ]	 = $host;
		$url_parts[ "path" ]	 = $path;
		$url_parts[ "file" ]	 = $file;
		$url_parts[ "query" ]	 = $query;
		$url_parts[ "domain" ]	 = $domain;
		$url_parts[ "port" ]	 = $port;

		$url_parts[ "auth_username" ]	 = $auth_username;
		$url_parts[ "auth_password" ]	 = $auth_password;

		return $url_parts;
	}

	function utf8_strrev( $str ) {
		preg_match_all( '/./us', $str, $ar );
		return join( '', array_reverse( $ar[ 0 ] ) );
	}

	function rebuildTLDs() {
		//get data 
		$source_url	 = "https://publicsuffix.org/list/effective_tld_names.dat";
		//$source_url = "http://localhost/effective_tld_names.dat";//debug 
		$lines		 = file( $source_url );

		//parse
		$regex = array();
		foreach ( $lines as $line ) {
			$line		 = trim( $line );
			if ( !$line OR substr( $line, 0, 2 ) == '//' )
				continue;
			$rev_line	 = mb_strtolower( $this->utf8_strrev( $line ), 'UTF-8' );
			$rev_line	 = preg_quote( $rev_line, '#' );
			if ( mb_substr( $rev_line, -2 ) == "\*" ) {
				$rev_line = mb_substr( $rev_line, 0, -2 ) . "[a-z0-9]+";
			}
			$regex[] = $rev_line;
		}
		// longer first
		usort( $regex, function ($a, $b) {
			return mb_strlen( $b, 'UTF-8' ) - mb_strlen( $a, 'UTF-8' );
		} );

		$masks		 = array();
		while ( $regex_part	 = array_splice( $regex, 0, 1000 ) )
			$masks[]	 = '#^(' . join( "|", $regex_part ) . ')\.#u';

		// everything is OK?
		if ( count( $masks ) > 5 ) {
			$tlds = array( 'updated_at' => time(), 'masks' => $masks );
			Helper::updateOption( 'tlds', $tlds );
		}
	}

	function initTLDs() {
		$current_tlds = Helper::getOption( 'tlds' );
		//$current_tlds = '';
		// not exists OR update week ago 
		if ( !$current_tlds OR time() - $current_tlds[ 'updated_at' ] > 7 * 24 * 3600 ) {
			$this->rebuildTLDs();
			$current_tlds = Helper::getOption( 'tlds' );
		}
		$this->regex_tld_masks = $current_tlds[ 'masks' ];
	}

	function getTLD( $domain ) {
		$rev_domain = mb_strtolower( $this->utf8_strrev( $domain ), 'UTF-8' );

		foreach ( $this->regex_tld_masks as $mask ) {
			if ( preg_match( $mask, $rev_domain, $m ) )
				return $this->utf8_strrev( $m[ 1 ] );
		}
		return "";
	}

}
