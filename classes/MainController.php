<?php

class MainController extends Controller {

	public $defaultAction = 'Entry';

	public function actionEntry() {
		$this->render( 'main' );
	}

	public function actionCrawls() {
		$settings		 = Helper::getOption( 'settings' );
		$crawler_status	 = Helper::getOption( 'crawler_status' );
		$current_crawl	 = $this->GetCurrentCrawl();
		$processes		 = $this->GetCurrentProcesses();
		if ( !$crawler_status )
			$crawler_status	 = 'Stop';
		$this->renderPartial( 'crawls', array( 'settings' => $settings, 'crawler_status' => $crawler_status, 'current_crawl' => $current_crawl, 'processes' => $processes ) );
	}

	public function GetCurrentProcesses() {
		$processes	 = array();
		$db			 = DB::getInstance();

		$r									 = $db->query( "SELECT count(*) FROM processes" );
		$processes[ 'running_processes' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='whois_available.php'" );
		$processes[ 'refresh_whois_avail' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='domainr_available.php'" );
		$processes[ 'refresh_domainr_avail' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='whois_expire_date.php'" );
		$processes[ 'refresh_whois_date' ]	 = $r->fetchColumn();
		$r									 = $db->query( "SELECT count(*) FROM processes WHERE type='majestic.php'" );
		$processes[ 'refresh_magestic' ]	 = $r->fetchColumn();
		$r->closeCursor();

		return $processes;
	}

	public function GetCurrentCrawl() {
		$current_crawl				 = array();
		$db							 = DB::getInstance();
		$r							 = $db->query( "SELECT title FROM crawls WHERE status='Processing' LIMIT 1" );
		$current_crawl[ 'title' ]	 = $r->fetchColumn();
		$r->closeCursor();
		return $current_crawl;
	}

	public function actionGetCurrentCrawlAndProcesses() {
		$crawl = $this->GetCurrentCrawl();

		$r = array();

		$processes					 = $this->GetCurrentProcesses();
		$r[ 'running_processes' ]	 = number_format( $processes[ 'running_processes' ] );
		$r[ 'refresh_whois_avail' ]	 = number_format( $processes[ 'refresh_whois_avail' ] );
		$r[ 'refresh_domainr_avail' ]	 = number_format( $processes[ 'refresh_domainr_avail' ] );
		$r[ 'refresh_whois_date' ]	 = number_format( $processes[ 'refresh_whois_date' ] );
		$r[ 'refresh_magestic' ]	 = number_format( $processes[ 'refresh_magestic' ] );

		echo json_encode( $r );
	}

	public function actionListCrawls() {

		$CrawlsTable = new CrawlsTable();
		$CrawlsTable->display();
	}

	public function actionSettings() {
		$settings = Helper::getOption( 'settings' );
		$this->renderPartial( 'settings', array( 'settings' => $settings ) );
	}

	public function actionSaveSettings() {
		$settings = $this->getRequest( 'settings' );
		if ( $settings ) {
			$db = DB::getInstance();
			if ( isset( $settings[ 'whois_tld_priority' ] ) ) {
				$settings[ 'whois_tld_priority' ] = preg_replace( '/[[:cntrl:]]+/', ' ', $settings[ 'whois_tld_priority' ] );

				$arr_priority	 = explode( " ", $settings[ 'whois_tld_priority' ] );
				$arr_priority	 = array_filter( $arr_priority );
				$arr_priority	 = array_map( "trim", $arr_priority );
				$arr_priority	 = array_unique( $arr_priority );
				
				$settings[ 'whois_tld_priority' ] = implode( ' ', $arr_priority );
			}
			Helper::updateOption( 'settings', $settings );
		}
	}

	public function actionSetCrawlerStatus() {
		$crawler_status = Helper::getOption( 'crawler_status' );
		switch ( $crawler_status ) {
			case 'Start':
				Helper::updateOption( 'crawler_status', 'Stop' );
				break;
			case 'Stop':
				Helper::updateOption( 'crawler_status', 'Start' );
				break;
			default :
				Helper::updateOption( 'crawler_status', 'Start' );
		}
	}

	public function actionAddCrawl() {
		$crawl = $this->getValue( $_REQUEST, 'crawl' );
		if ( $crawl ) {
			$urls		 = $crawl[ 'list_urls' ];
			$arr_urls	 = explode( "\n", $urls );
			$arr_urls	 = array_unique( $arr_urls );
			$arr_urls	 = array_map( "trim", $arr_urls );
			$arr_urls	 = array_filter( $arr_urls );
			$crawler	 = new Crawler();
			$crawler->uploadUrlsAddCrawl( $arr_urls );
		}
		header( 'Location: ?act=Entry' );
	}

	public function actionDeleteCrawl() {
		$crawl_id = $this->getRequest( 'crawl_id' );
		if ( $crawl_id ) {
			$db = DB::getInstance();

			$pr	 = $db->prepare( "SELECT * FROM crawls WHERE id=:crawl_id" );
			$r	 = $pr->execute( array( ':crawl_id' => $crawl_id ) );
			$row = $pr->fetch( PDO::FETCH_NAMED );
			if ( $row[ 'status' ] == 'Processing' )
				Helper::finish_crawl( $db );

			$pr		 = $db->prepare( 'DELETE FROM crawls WHERE id=:crawl_id' );
			$r		 = $pr->execute( array( ':crawl_id' => $crawl_id ) );
			$crawler = new Crawler();
			$crawler->cancelCrawl( $row[ 'title' ] );
			$r->closeCursor();
			echo $r;
		}
	}

	public function actionDomains() {
		$this->renderPartial( 'domains' );
	}

	public function actionDomainsAvail() {
		$db = DB::getInstance();

		$pr		 = $db->prepare( "SELECT * FROM majestic_topics ORDER BY title" );
		$r		 = $pr->execute();
		$topics	 = $pr->fetchAll( PDO::FETCH_NAMED );
		$this->renderPartial( 'DomainsAvail', array( 'topics' => $topics ) );
	}

	public function actionListDomains() {
		$DomainsTable = new DomainsTable();
		$DomainsTable->display();
	}

	public function actionListDomainsAvail() {
		$DomainsTable = new DomainsAvail();
		$DomainsTable->display();
	}

}
