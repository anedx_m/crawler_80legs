<?php

class Crawler80Legs {

	private $base_url;
	private $curl_info;

	public function __construct() {
		$token			 = Helper::getSetting( '80legs_api' );
		$this->base_url	 = "https://$token@api.80legs.com/v2/";
	}

	public function request( $method, $url, $data = '', $header = array(), $connection_timeout = 30 ) {
		$url = $this->base_url . $url;
		if ( empty( $header ) ) {
			$header = array(
				"Content-Type: application/octet-stream",
			);
		}

		$curl_options	 = array(
			CURLOPT_CUSTOMREQUEST	 => $method,
			CURLOPT_POSTFIELDS		 => $data,
			CURLOPT_RETURNTRANSFER	 => 1,
			CURLOPT_BINARYTRANSFER	 => 1,
			CURLOPT_CONNECTTIMEOUT	 => $connection_timeout,
			CURLOPT_TIMEOUT			 => $connection_timeout,
			CURLOPT_USERAGENT		 => 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.1) Gecko/20090716 Ubuntu/9.04 (jaunty) Shiretoko/3.5.1',
			CURLOPT_VERBOSE			 => 0, //was 2 
			CURLOPT_HEADER			 => 0,
//			CURLOPT_FOLLOWLOCATION	 => 1,
//			CURLOPT_MAXREDIRS		 => 5,
			CURLOPT_AUTOREFERER		 => 1,
			CURLOPT_FRESH_CONNECT	 => 1,
			CURLOPT_HTTPHEADER		 => $header,
			CURLOPT_SSL_VERIFYPEER	 => false,
			CURLOPT_SSL_VERIFYHOST	 => false,
		);
		$ch				 = curl_init( $url );
		curl_setopt_array( $ch, $curl_options );
		$r				 = curl_exec( $ch );
		$this->curl_info = curl_getinfo( $ch );
		curl_close( $ch );

		return $r;
	}

	public function uploadUrls( $list_name, $list_urls ) {
		$r = $this->request( 'PUT', "urllists/$list_name", json_encode( $list_urls ) );
		return $r;
	}

	public function getUrlList( $list_name ) {
		$r = $this->request( 'GET', "urllists/$list_name" );
		return $r;
	}

	public function deleteUrlList( $list_name ) {
		$r = $this->request( 'DELETE', "urllists/$list_name" );
		return $r;
	}

	public function upload80app( $filename ) {
		$file	 = file_get_contents( $filename );
		$b_name	 = basename( $filename );
		$r		 = $this->request( 'PUT', "apps/$b_name", $file );
		return $r;
	}

	public function createCrawl( $crawls_name, $app_name, $url_list_name, $data_file_name, $max_depth, $max_urls ) {
		$crawl = array(
			"app"		 => $app_name,
			"urllist"	 => $url_list_name,
			"max_depth"	 => $max_depth,
			"max_urls"	 => $max_urls
		);
		if ( !empty( $data_file_name ) ) {
			$crawl[ "data" ] = $data_file_name;
		}
		$header	 = array(
			'Content-Type: application/json'
		);
		$r		 = $this->request( 'POST', "crawls/$crawls_name", json_encode( $crawl ), $header );
		return $r;
	}

	public function getAllCrawls() {
		$r = $this->request( 'GET', "crawls" );
		return $r;
	}

	public function getCrawlStatus( $crawl_name ) {
		$r = $this->request( 'GET', "crawls/$crawl_name" );
		return $r;
	}

	public function cancelCrawl( $crawl_name ) {
		$r = $this->request( 'DELETE', "crawls/$crawls_name" );
		return $r;
	}

	public function getResultsCrawl( $crawls_name ) {
		$r = $this->request( 'GET', "results/$crawls_name" );
		return $r;
	}

	private function getDescriptionAnsw( $error_code ) {
		$answ	 = array(
			200		 => 'Request was successful',
			204		 => 'Request was successful, but no data was returned (This is expected behavior)',
			400		 => 'Bad Request	Often times a missing or incorrect parameter',
			401		 => 'Invalid/Missing API Token',
			404		 => 'Using an invalid API end point, or the user supplied path is incorrect',
			415		 => 'Content type is not set / set incorrectly.',
			422		 => 'All the parameters were correct but the request was rejected on the back end, contact support with request information',
			523		 => 'The API is currently unavailable due to maitenance, try again later',
			'5xx'	 => 'There was an error within the Datafiniti servers',
		);
		$keys	 = array_keys( $answ );
		if ( in_array( $error_code, $keys ) ) {
			return $answ[ $error_code ];
		} elseif ( substr( $error_code, 1, 1 ) == 5 ) {
			return $answ[ '5xx' ];
		}
		return '';
	}

	public function getLastCurlInfo() {
		return $this->curl_info;
	}

	public function getLastAnsw() {
		if ( is_array( $this->curl_info ) AND isset( $this->curl_info[ 'http_code' ] ) ) {
			return $this->getDescriptionAnsw( $this->curl_info[ 'http_code' ] );
		}
		return 'Unknown error';
	}

	public function getLastHTTPCODE() {
		if ( is_array( $this->curl_info ) AND isset( $this->curl_info[ 'http_code' ] ) ) {
			return $this->curl_info[ 'http_code' ];
		}
		return false;
	}

	

	
}
