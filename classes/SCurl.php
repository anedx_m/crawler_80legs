<?php

class SCurl {

	private $curl_info;

	public function request( $url, $data = null, $curl_options = array(), $header = null, $connection_timeout = 30 ) {

		if ( is_null( $header ) ) {
			$header = array(
				"Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
				"Accept-Language: en-us",
				"Accept-Charset: SO-8859-1,utf-8;q=0.7,*;q=0.7",
				"Keep-Alive: 300",
				"Connection: keep-alive",
				"Pragma: no-cache",
				"Cache-Control: no-cache",
				"Expect:",
			);
		}

		$default_curl_options = array(
			CURLOPT_RETURNTRANSFER	 => 1,
			CURLOPT_BINARYTRANSFER	 => 1,
			CURLOPT_CONNECTTIMEOUT	 => $connection_timeout,
			CURLOPT_TIMEOUT			 => $connection_timeout,
			CURLOPT_USERAGENT		 => 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.1.1) Gecko/20090716 Ubuntu/9.04 (jaunty) Shiretoko/3.5.1',
			CURLOPT_VERBOSE			 => 0, //was 2 
			CURLOPT_HEADER			 => 0,
//			CURLOPT_FOLLOWLOCATION	 => 1,
//			CURLOPT_MAXREDIRS		 => 5,
			CURLOPT_AUTOREFERER		 => 1,
			CURLOPT_FRESH_CONNECT	 => 1,
			CURLOPT_HTTPHEADER		 => $header,
			CURLOPT_SSL_VERIFYPEER	 => false,
			CURLOPT_SSL_VERIFYHOST	 => false,
		);
		if ( $data ) {
			$default_curl_options[ CURLOPT_POSTFIELDS ] = $data;
		}
		
		foreach ( $curl_options as $option => $value ) {
			$default_curl_options[ $option ] = $value;
		}
		var_dump( $header );

		$ch				 = curl_init( $url );
		curl_setopt_array( $ch, $default_curl_options );
		$r				 = curl_exec( $ch );
		$this->curl_info = curl_getinfo( $ch );
		curl_close( $ch );

		return $r;
	}

	public function getLastCurlInfo() {
		return $this->curl_info;
	}

}
