<?php

class WhoisAPIAvailable extends SCurl {

	public function processTakenNextDomain() {
		$whoiskey = Helper::getSetting( 'whois_api_key' );

		$tlds			 = Helper::getSetting( 'whois_tld_priority' );
		$order_by_tlds	 = '';
		if ( $tlds ) {
			$tlds	 = explode( ' ', $tlds );
			$tlds    = array_reverse($tlds);
			$s_tlds	 = array_map( function($value) {
				return "'$value'";
			}, $tlds );
			$tlds			 = implode( ', ', $s_tlds );
			$order_by_tlds	 = "ORDER BY FIELD(tld, $tlds,'' DESC ";
		}

		$db	 = DB::getInstance();
		$pr	 = $db->prepare( "SELECT * FROM domains_all WHERE whois_check_avail_date IS NULL $order_by_tlds LIMIT 1" ); //debug 
		var_dump( "SELECT * FROM domains_all WHERE whois_check_avail_date IS NULL $order_by_tlds LIMIT 1" );
		$r	 = $pr->execute();



		$domains = $pr->fetchAll( PDO::FETCH_NAMED );
		var_dump( $domains );
		foreach ( $domains as $domain ) {
			$url		 = $this->getWhoisUrl( 'taken', $domain[ 'domain' ], $whoiskey );
			$content	 = $this->request( $url );
			$curl_info	 = $this->getLastCurlInfo();
			var_dump( $content );
			if ( $curl_info[ 'http_code' ] == 200 ) {
				$db			 = DB::getInstance();
				$data		 = json_decode( $content, true );
				$domain_id	 = $domain[ 'id' ];
				$domain		 = $domain[ 'domain' ];

				if ( $data[ 'status' ] == 0 AND isset( $data[ 'taken' ] ) ) {
					$whois_available = $data[ 'taken' ] ? 0 : 1; // revert value
					$pr				 = $db->prepare( "UPDATE domains_all SET whois_available=:whois_available, whois_check_avail_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );
					$pr->execute( array( ':domain_id' => $domain_id, ':whois_available' => $whois_available ) );
					if ( $whois_available ) {
						$crawler = new Crawler();
						$tld	 = $crawler->getTLD( $domain );
						$pr		 = $db->prepare( "INSERT IGNORE domains_avail (domain,date_found,tld) VALUES (:domain,CURRENT_TIMESTAMP,:tld)" );
						$pr->execute( array( ':domain' => $domain, ':tld' => $tld ) );
					}
				} else {
					$pr = $db->prepare( "UPDATE domains_all SET whois_check_avail_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );
					$pr->execute( array( ':domain_id' => $domain_id ) );
				}
			}
		}
		return count( $domains );
	}

	public function processWhoisNextDomain() {
		$whoiskey = Helper::getSetting( 'whois_api_key' );

		$db		 = DB::getInstance();
		$pr		 = $db->prepare( "SELECT * FROM domains_all WHERE whois_check_expiry_date IS NULL LIMIT 1" ); //debug 
		$r		 = $pr->execute();
		$domains = $pr->fetchAll( PDO::FETCH_NAMED );
		var_dump( $domains );
		foreach ( $domains as $domain ) {
			$url		 = $this->getWhoisUrl( 'whois', $domain[ 'domain' ], $whoiskey );
			$content	 = $this->request( $url );
			$curl_info	 = $this->getLastCurlInfo();
			if ( $curl_info[ 'http_code' ] == 200 ) {
				$db			 = DB::getInstance();
				$data		 = json_decode( $content, true );
				var_dump( $data );
				$domain_id	 = $domain[ 'id' ];
				$domain		 = $domain[ 'domain' ];
//				if ( $data[ 'status' ] == 0 AND isset( $data[ 'date_expires' ] ) ) {
				if ( isset( $data[ 'date_expires' ] ) ) {
					$date_expire = $data[ 'date_expires' ];
					$pr			 = $db->prepare( "UPDATE domains_all SET date_expire=:date_expire, whois_check_expiry_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );
					$pr->execute( array( ':domain_id' => $domain_id, ':date_expire' => $date_expire ) );
				} else {
					$pr = $db->prepare( "UPDATE domains_all SET  whois_check_expiry_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );
					$pr->execute( array( ':domain_id' => $domain_id ) );
				}
			} else {
				$pr = $db->prepare( "UPDATE domains_all SET  whois_check_expiry_date=CURRENT_TIMESTAMP WHERE id=:domain_id" );
				$pr->execute( array( ':domain_id' => $domain_id ) );
			}
		}
		return count( $domains );
	}

	public function getWhoisUrl( $type, $domain, $key ) {
		return "http://api.whoapi.com/?domain=$domain&r=$type&apikey=$key";
	}

}
