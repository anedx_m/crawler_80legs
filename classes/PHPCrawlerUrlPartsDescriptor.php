<?php

/**
 * Describes the single parts of an URL.
 *
 * @package phpcrawl
 * @internal
 */
class PHPCrawlerUrlPartsDescriptor {

	public $protocol;
	public $host;
	public $path;
	public $file;
	public $domain;
	public $port;
	public $auth_username;
	public $auth_password;

	/**
	 * Returns the PHPCrawlerUrlPartsDescriptor-object for the given URL.
	 *
	 * @return PHPCrawlerUrlPartsDescriptor
	 */
	public static function fromURL( $url ) {
		$parts = self::splitURL( $url );

		$tmp = new PHPCrawlerUrlPartsDescriptor();

		$tmp->protocol		 = $parts[ "protocol" ];
		$tmp->host			 = $parts[ "host" ];
		$tmp->path			 = $parts[ "path" ];
		$tmp->file			 = $parts[ "file" ];
		$tmp->domain		 = $parts[ "domain" ];
		$tmp->port			 = $parts[ "port" ];
		$tmp->auth_username	 = $parts[ "auth_username" ];
		$tmp->auth_password	 = $parts[ "auth_password" ];

		return $tmp;
	}

	public function toArray() {
		return get_object_vars( $this );
	}

	public static function splitURL( $url ) {
		// Protokoll der URL hinzuf�gen (da ansonsten parse_url nicht klarkommt)
		if ( !preg_match( "#^[a-z]+://# i", $url ) )
			$url = "http://" . $url;

		$parts = @parse_url( $url );

		if ( !isset( $parts ) )
			return null;

		$protocol		 = $parts[ "scheme" ] . "://";
		$host			 = (isset( $parts[ "host" ] ) ? $parts[ "host" ] : "");
		$path			 = (isset( $parts[ "path" ] ) ? $parts[ "path" ] : "");
		$query			 = (isset( $parts[ "query" ] ) ? "?" . $parts[ "query" ] : "");
		$auth_username	 = (isset( $parts[ "user" ] ) ? $parts[ "user" ] : "");
		$auth_password	 = (isset( $parts[ "pass" ] ) ? $parts[ "pass" ] : "");
		$port			 = (isset( $parts[ "port" ] ) ? $parts[ "port" ] : "");

		// File
		preg_match( "#^(.*/)([^/]*)$#", $path, $match ); // Alles ab dem letzten "/"
		if ( isset( $match[ 0 ] ) ) {
			$file	 = trim( $match[ 2 ] );
			$path	 = trim( $match[ 1 ] );
		} else {
			$file = "";
		}

		// Der Domainname aus dem Host
		// Host: www.foo.com -> Domain: foo.com
		$parts = @explode( ".", $host );
		if ( count( $parts ) <= 2 ) {
			$domain = $host;
		} else if ( preg_match( "#^[0-9]+$#", str_replace( ".", "", $host ) ) ) { // IP
			$domain = $host;
		} else {
			$pos	 = strpos( $host, "." );
			$domain	 = substr( $host, $pos + 1 );
		}

		// DEFAULT VALUES f�r protocol, path, port etc. (wenn noch nicht gesetzt)
		// Wenn Protokoll leer -> Protokoll ist "http://"
		if ( $protocol == "" )
			$protocol = "http://";

		// Wenn Port leer -> Port setzen auf 80 or 443
		// (abh�ngig vom Protokoll)
		if ( $port == "" ) {
			if ( strtolower( $protocol ) == "http://" )
				$port	 = 80;
			if ( strtolower( $protocol ) == "https://" )
				$port	 = 443;
		}

		// Wenn Pfad leet -> Pfad ist "/"
		if ( $path == "" )
			$path = "/";

		// R�ckgabe-Array
		$url_parts[ "protocol" ]	 = $protocol;
		$url_parts[ "host" ]		 = $host;
		$url_parts[ "path" ]		 = $path;
		$url_parts[ "file" ]		 = $file;
		$url_parts[ "query" ]		 = $query;
		$url_parts[ "domain" ]	 = $domain;
		$url_parts[ "port" ]		 = $port;

		$url_parts[ "auth_username" ]	 = $auth_username;
		$url_parts[ "auth_password" ]	 = $auth_password;

		return $url_parts;
	}

}

?>