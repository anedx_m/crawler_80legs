CREATE OR REPLACE RULE domains_all_table_ignore_duplicate_inserts AS
    ON INSERT TO domains_all
   WHERE (EXISTS ( SELECT 1
           FROM domains_all
          WHERE domains_all.domain = NEW.domain)) DO INSTEAD NOTHING;
          
CREATE OR REPLACE RULE domains_job_table_ignore_duplicate_inserts AS
    ON INSERT TO domains_job
   WHERE (EXISTS ( SELECT 1
           FROM domains_job
          WHERE domains_job.domain = NEW.domain)) DO INSTEAD NOTHING;
          
CREATE OR REPLACE RULE urls_table_ignore_duplicate_inserts AS
    ON INSERT TO urls
   WHERE (EXISTS ( SELECT 1
           FROM urls
          WHERE urls.url= url)) DO INSTEAD NOTHING;                    