<form class="DomainsAvail-params">
	<div class="row">
		<div class="col-lg-3">
			<div class="padding-10">
				<input id="search-input" class="input-sm search-input" placeholder="search" name="search">
			</div>
		</div>
		<div class="col-lg-3">
			<div class="padding-10">
				<input id="date_from" class="input-sm date_from" placeholder="Date from" name="date_from">
				<input id="date_to" class="input-sm date_to" placeholder="Date to" name="date_to">
			</div>
		</div>

		<div class="col-lg-6">
			<div class="padding-10">
				<select class="input-sm" name="topic">
					<option></option>
					<?php
					foreach ( $topics as $topic ) {
						?>
						<option value="<?php echo $topic[ 'id' ] ?>"><?php echo $topic[ 'title' ] ?></option>
						<?php
					}
					?>
				</select>
			</div>
		</div>
	</div>

</form>
<div class="DomainsAvail-table"></div>

<script>
	var ex_DomainsAvail = new Table( '?act=ListDomainsAvail', '.DomainsAvail-table' );

	$( document ).ready( function() {
		$( '#date_from,#date_to' ).datepicker( { dateFormat: 'yy-mm-dd' } );
		ex_DomainsAvail.list( { order_by: 'domainr_check_time', direction: 'DESC' } )
		$( '#filter-domain' ).click( function() {
			var params = getParams();
			ex_DomainsAvail.list( params, 1 );
		} )
		
	} )
	function search() {
		var params = getParams();
		ex_DomainsAvail.list( params, 1 );
	}
	function getParams() {
		var inps = $( '.DomainsAvail-params input,select' );
		var params = { };
		$.each( inps, function( i, el ) {
			var name = $( el ).attr( 'name' )
			var type = $( el ).attr( 'type' )
			if ( name ) {
				if ( type == 'checkbox' ) {
					params[name] = $( el ).is( ':checked' )
				} else
					params[name] = $( el ).val();
			}
		} )
		console.log( params )
		return params;
	}
	$( '.DomainsAvail-params input:not([type=checkbox],.date_from,.date_to)' ).afterKeyDown( function( ev ) {
		search()
	}, 1000, true );
	$( '.DomainsAvail-params input' ).afterChange( function( ev ) {
		search()
	}, 1000, true );
	$( '.DomainsAvail-params select' ).afterChange( function( ev ) {
		search()
	}, 1000, true );
</script>