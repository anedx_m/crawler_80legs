
<div class="crawl-actions">
	<?php
	if ( $crawler_status == 'Stop' ) {
		$class_action			 = 'btn-success';
		$class_action_text		 = 'Start';
		$class_action_text_state = 'Stopped';
	} else {
		$class_action			 = 'btn-danger';
		$class_action_text		 = 'Stop';
		$class_action_text_state = 'Running';
	}
	?>
	<div class="row">
		<div class="col-sm-1">
			<span style="font-size: 15px; font-weight:bold;"><?php echo $class_action_text_state ?></span> <input type="button" class="btn btn-default crawler_status <?php echo $class_action ?>" value="<?php echo $class_action_text ?>">
		</div>

		<div class="col-sm-7">
			<div class="btn-hs-add-crawl btn btn-default"><div class="text-hs-add-crawl fa-chevron-down-after">ADD CRAWL</div></div>
			<div class="add-new-crawl" style="display: none;">
				<form action="?act=AddCrawl" method="POST">
					<input type="hidden" name="MAX_FILE_SIZE" value="30000" />
					<div class="row">
						
						<div class="col-sm-10">
							<div class="row padding-2">
								<div class="col-sm-3 text-right">
									<label for="fileCrawl">Crawl Domains/Urls: </label>
								</div>
								<div class="col-sm-9">
									<textarea class="list-urls" name="crawl[list_urls]"></textarea>
								</div>
							</div>
							<div class="row padding-2">
								<div class="col-sm-offset-6 col-sm-6">
									<input type="submit" class="btn btn-primary" value="Add Crawl">
								</div>
							</div>
						</div>
					</div>

				</form>
			</div>

		</div>
		<div class="col-sm-4">
			<?php if ( !empty( $current_crawl ) ): ?>
				<div style="float:	">
				
			<?php if ( !empty( $current_crawl[ 'title' ] ) ): ?>
					<h4>Current Crawl:</h4>
					<table class="table table-hover table-bordered table-striped">
						<tr>
							<th>Current Crawl</th>
							<th>Urls in queue</th>
							<th>Passed urls</th>
							<th>Domains found</th>
						</tr>
						<tr>
							<td class="current_crawl"><?php echo $current_crawl[ 'title' ] ?></td>
						</tr>
					</table>
			<?php endif; ?>
					<div class="btn btn-default slide-processes"><div class="fa-chevron-down-after">View Status</div></div>
					<div class="d-processes">
						<table class="table table-hover table-bordered table-striped processes">
							<tr>
								<td>
									Running Processes:
								</td>
								<td class="running_processes">
									<?php echo $processes[ 'running_processes' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Whois Avail:
								</td>
								<td  class="refresh_whois_avail">
									<?php echo $processes[ 'refresh_whois_avail' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Domainr Avail:
								</td>
								<td  class="refresh_domainr_avail">
									<?php echo $processes[ 'refresh_domainr_avail' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Whois Date:
								</td>
								<td class="refresh_whois_date">
									<?php echo $processes[ 'refresh_whois_date' ] ?>
								</td>
							</tr>
							<tr>
								<td>
									Refresh Magestic:
								</td>
								<td class="refresh_magestic">
									<?php echo $processes[ 'refresh_magestic' ] ?>
								</td>
							</tr>
						</table>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="search padding-10">
	<input id="search-input" class="input-sm" placeholder="search">
	<input id="search-btn" type="button" class="btn btn-default" value="search">
</div>
<div class="crawls-table"></div>
<script>
	var ex_crawls = new Table( '?act=ListCrawls', '.crawls-table' );

	$( document ).ready( function() {
		ex_crawls.list( { order_by: 'status' } )
		$( '#search-btn' ).click( function() {
			var params = { search: $( '#search-input' ).val( ) }
			ex_crawls.list( params, 1 );
		} )
		ex_crawls.afterList.add( function() {
		
			$( '.crawls-table .delete' ).click( function() {
				var crawl_id = $( this ).attr( 'id' );
				var r = confirm( 'Are you sure?' )
				if ( r ) {
					$.post( '?act=DeleteCrawl', { 'crawl_id': crawl_id }, function() {
						ex_crawls.list()
					} )

				}
			} )
			
			$( '.crawls-table .stop' ).click( function() {
				var crawl_id = $( this ).attr( 'id' );
				var r = confirm( 'Are you sure?' )
				if ( r ) {
					$.post( '?act=StopCrawl', { 'crawl_id': crawl_id }, function() {
						ex_crawls.list()
					} )

				}
			} )
			
			$( '.crawler_status' ).click( function() {
				$.post( '?act=SetCrawlerStatus', '', function( data ) {
//					alert(data)
					document.location.reload()
				} )
			} )
		} )
		$( '.btn-hs-add-crawl' ).click( function() {
			$( ".add-new-crawl" ).slideToggle( 'slow' );
		} )
		
<?php if ( !empty( $current_crawl ) ): ?>
			if ( typeof t !== 'undefined' )
				clearInterval( t );
			t = setInterval( refresh_current_crawl_processes, 10000 )
<?php endif; ?>
			
			$( '.d-processes' ).toggle()
			$( '.slide-processes' ).click( function() {
				$( '.d-processes' ).slideToggle( 'slow' );
			} )

			function refresh_current_crawl_processes() {
				$.post( '?act=GetCurrentCrawlAndProcesses', '', function( data ) {
					$( '.current_crawl' ).html( data.current_crawl )
	
				$( '.running_processes' ).html( data.running_processes )
					$( '.refresh_whois_avail' ).html( data.refresh_whois_avail )
					$( '.refresh_domainr_avail' ).html( data.refresh_domainr_avail )
					$( '.refresh_whois_date' ).html( data.refresh_whois_date )
					$( '.refresh_magestic' ).html( data.refresh_magestic )
				}, 'JSON' )
			}
	} )



</script>