<ul class="nav nav-tabs main-tabs">
	<li class="active"><a href="#Crawls">Crawls</a></li>
	<li><a href="#Domains">Domains All</a></li>
	<li><a href="#DomainsAvail">Domains Available</a></li>
	<li><a href="#Settings">Settings</a></li>
</ul>
<div class="tab-content main-tab-content">
    <div class="tab-pane active" id="Crawls"></div>
	<div class="tab-pane" id="Domains"></div>
	<div class="tab-pane" id="DomainsAvail"></div>
	<div class="tab-pane" id="Settings"></div>
</div>
<script>
	$( document ).ready( function() {
		$( '.main-tabs a' ).click( function( e ) {
			e.preventDefault();
			var action = $( this ).attr( 'href' ).substr( 1 );
			var that = this
			$( '.main-tab-content div' ).empty();
			$( this ).tab( 'show' )
			$.post( '?act=' + action, '', function( data ) {
				$( '.main-tab-content div.active' ).html( data )
			}, 'HTML' )
		} )
		$( '.main-tabs .active a' ).click();

	} )
</script>