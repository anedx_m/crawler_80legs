
<form class="settings-form form-horizontal">
	<div class="settings row">
		<div class="setting col-sm-3">
			<h4 class="header">General</h4>
			<label for="password">Password:</label><input name="settings[password]" placeholder="Password" id="password" type="text" class="" value="<?php echo $this->getValue( $settings, 'password' ) ?>">
		</div>
		<div class="setting col-sm-4">
			<h4 class="header">80 legs</h4>
			<label for="80legs_api">80legs api token:</label><input name="settings[80legs_api]" placeholder="80legs api" id="80legs_api" type="text" class="" value="<?php echo $this->getValue( $settings, '80legs_api' ) ?>">
			<label for="max_depth">Max depth:</label><input name="settings[max_depth]" placeholder="max depth" id="max_depth" type="text" class="" value="<?php echo $this->getValue( $settings, 'max_depth' ) ?>">
			<label for="domains_per_crawl">Domains per crawl:</label><input name="settings[domains_per_crawl]" placeholder="Domains per crawl" id="domains_per_crawl" type="text" class="" value="<?php echo $this->getValue( $settings, 'domains_per_crawl' ) ?>">
			<label for="max_urls_per_crawl">Max urls per crawl:</label><input name="settings[max_urls_per_crawl]" placeholder="Max urls per crawl" id="max_urls_per_crawl" type="text" class="" value="<?php echo $this->getValue( $settings, 'max_urls_per_crawl' ) ?>">
			<label for="max_crawls">Max crawls:</label><input name="settings[max_crawls]" placeholder="Max crawls" id="max_crawls" type="text" class="" value="<?php echo $this->getValue( $settings, 'max_crawls' ) ?>">
			<label for="crawl_prefix">Crawl Prefix:</label><input name="settings[crawl_prefix]" placeholder="Crawl prefix" id="crawl_prefix" type="text" class="" value="<?php echo $this->getValue( $settings, 'crawl_prefix' ) ?>">
			<label for="display_first_x_topics">Display First X Topics:</label><input name="settings[display_first_x_topics]" placeholder="Display First X Topics" id="display_first_x_topics" type="text" class="" value="<?php echo $this->getValue( $settings, 'display_first_x_topics' ) ?>">
		</div>

	</div>
	<div class="settings row">
		<div class="setting col-sm-3">
			<h4 class="header">Whois</h4>
			<label for="whois_api_key">Api key:</label><input name="settings[whois_api_key]" placeholder="Whois api key" id="whois_api_key" type="text" class="" value="<?php echo $this->getValue( $settings, 'whois_api_key' ) ?>">
			<label for="taken_per_minute">"taken" per minute:</label><input name="settings[taken_per_minute]" placeholder="'taken' per minute" id="refresh_whois" type="text" class="" value="<?php echo $this->getValue( $settings, 'taken_per_minute' ) ?>">
			<label for="whois_per_minute">"whois" per minute:</label><input name="settings[whois_per_minute]" placeholder="'whois' per minute" id="whois_per_minute" type="text" class="" value="<?php echo $this->getValue( $settings, 'whois_per_minute' ) ?>">
			<label for="whois_tld_priority">tld priority:</label><textarea name="settings[whois_tld_priority]" placeholder="priority" id="whois_tld_priority" type="text" class="" value=""><?php echo $this->getValue( $settings, 'whois_tld_priority' ) ?></textarea>
			<label for="mashape_key">mashape key</label><input name="settings[mashape_key]" placeholder="mashape key" id="mashape_key" type="text" class="" value="<?php echo $this->getValue( $settings, 'mashape_key' ) ?>">
			
		</div>
		<div class="setting col-sm-4">
			<h4 class="header">Majestic</h4>
			<label for="api_key">Api key:</label><input name="settings[ms_api_key]" placeholder="Api key" id="min_moz_da" type="text" class="" value="<?php echo $this->getValue( $settings, 'ms_api_key' ) ?>">
		</div>
	</div>
</form>
<div class="clearfix"></div>
<div class="row">
	<div class="col-sm-2 ">
		<input type="button" class="btn btn-primary" id="settings-save" value="Save">
	</div>
</div>


<script>
	$( '#settings-save' ).click( function() {
		$.post( '?act=SaveSettings', $( '.settings-form' ).serialize(), function() {
			alert( "Settings updated" )
		} )
	} )
</script>
